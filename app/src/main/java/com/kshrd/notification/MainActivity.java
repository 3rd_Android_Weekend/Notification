package com.kshrd.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.renderscript.RenderScript;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.widget.RemoteViews;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnSimpleNotification)
    void onSimpleNotificationClicked() {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("name", "John");
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.jingle_bell);

        long[] pattern = {500, 500, 500, 500, 500};

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder
                .setContentTitle("Content Title")
                .setContentText("Content Text")
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.fb))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setSound(sound)
                .setLights(Color.RED, 1, 1)
                .setVibrate(pattern)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        Notification notification = builder.build();
        int id = (int) (System.currentTimeMillis() / 1000);
        notificationManager.notify(id, notification);

    }

    @OnClick(R.id.btnInboxStyle)
    public void onInboxStyleClicked() {

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        String[] events = new String[3];
        events[0] = new String("1) Message for implicit intent");
        events[1] = new String("2) big view Notification");
        events[2] = new String("3) from HRD!");
        // Sets a title for the Inbox style big view
        inboxStyle.setBigContentTitle("More Details:");
        // Moves events into the big view
        for (int i = 0; i < events.length; i++) {
            inboxStyle.addLine(events[i]);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.fb))
                .setStyle(inboxStyle)
                .setSmallIcon(R.mipmap.ic_launcher);

        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        Notification notification = builder.build();
        int id = (int) (System.currentTimeMillis() / 1000);
        notificationManager.notify(id, notification);

    }

    @OnClick(R.id.btnBigPictureStyle)
    public void onBigPictureStyleClicked() {

        NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle();
        bigPicStyle.bigPicture(BitmapFactory.decodeResource(getResources(), R.drawable.bg));
        bigPicStyle.bigLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.fb));
        bigPicStyle.setBigContentTitle("New Promotion!");
        bigPicStyle.setSummaryText("Summary Text");

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.fb))
                .setStyle(bigPicStyle)
                .setSmallIcon(R.mipmap.ic_launcher);

        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        Notification notification = builder.build();
        int id = (int) (System.currentTimeMillis() / 1000);
        notificationManager.notify(id, notification);


    }

    @OnClick(R.id.btnBigTextStyle)
    public void onBigtextStyleClicked() {
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.bigText("Some Text!");
        bigTextStyle.setBigContentTitle("Hello everyone !");
        bigTextStyle.setSummaryText("Summary Text !");

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder
                .setContentTitle("Hello everyone !")
                .setContentText("Some Text!")
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.fb))
                .setStyle(bigTextStyle)
                .setSmallIcon(R.mipmap.ic_launcher);

        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        Notification notification = builder.build();
        int id = (int) (System.currentTimeMillis() / 1000);
        notificationManager.notify(id, notification);
    }

    @OnClick(R.id.btnCustomNotification)
    public void onCustomNotificationClicked() {

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://google.com"));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.custom_notification);

        builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker("Bonjour!")
                .setAutoCancel(true)
                .setContent(remoteViews);

        remoteViews.setTextViewText(R.id.tvTitle, "Hello world !");
        remoteViews.setImageViewResource(R.id.ivIcon, R.drawable.fb);

        NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        int id = (int) (System.currentTimeMillis() / 1000);
        notificationmanager.notify(id, builder.build());

    }
}
