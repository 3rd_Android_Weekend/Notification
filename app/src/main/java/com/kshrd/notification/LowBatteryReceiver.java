package com.kshrd.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

/**
 * Created by pirang on 7/2/17.
 */

public class LowBatteryReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, DetailActivity.class);
        intent.putExtra("name", "John");
        PendingIntent pendingIntent = PendingIntent.getActivity(
                context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.jingle_bell);

        long[] pattern = {500, 500, 500, 500, 500};

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder
                .setContentTitle("Content Title")
                .setContentText("Content Text")
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.fb))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setSound(sound)
                .setLights(Color.RED, 1, 1)
                .setVibrate(pattern)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                Context.NOTIFICATION_SERVICE);
        Notification notification = builder.build();
        int id = (int) (System.currentTimeMillis() / 1000);
        notificationManager.notify(id, notification);
    }
}
